import React from 'react';
import axios from 'axios';

class UserAdd extends React.Component {

    state = { 
            name: '',
            age: ''
    }

    resetForm = () => {
        this.setState({name:'',age:''});
    }

    submitFormHandler = (event) => {
        console.log(this.state);
        axios.post('/api/users', {name:this.state.name,age:this.state.age}).then((data)=>{
            this.props.onUserAdd(data);
            this.resetForm();
        }).catch((error)=>{
            console.log('error on adding user', error);
        });
        event.preventDefault();
    }

    inputChangeHandler = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    render() {
        return (
            <form onSubmit={this.submitFormHandler}>
                <p>
                <label>Name</label>
                <input type='text' name='name' onChange={this.inputChangeHandler} value={this.state.name} ></input>
                </p>
                <p>
                <label>Age</label>
                <input type='text' name='age' onChange={this.inputChangeHandler}  value={this.state.age}></input>
                </p>
                <input type='submit' value='Add'></input> 
            </form>
        );
    }
}

export default UserAdd;