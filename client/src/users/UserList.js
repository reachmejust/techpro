import React from 'react';
import axios from 'axios';
import UserInfo from './UserInfo';
import UserAdd from './UserAdd';

class UserList extends React.Component {

    state = {
        users: []
    }

    componentDidMount() {
        this.getUsers();
    }

    getUsers(){
        axios.get('/api/users').then((response)=>{
            this.setState({users:response.data});
        });
    }

    userAddHandler = (response) => {
        const userList = [...this.state.users];
        userList.push(response.data);
        this.setState({users:userList});
    }

    removeUserHandler = (event, userid) => {
        event.preventDefault();

        axios.delete(`/api/users/${userid}`).then(response=>{
            this.getUsers();
        });
        
    }

    render() {

        const users = this.state.users.map((user, index)=>{
            return <li key={user._id}><UserInfo name={user.name} age={user.age} onRemoveUser={(e) => this.removeUserHandler(e, user._id)}/></li>
        });

        return (
            <div>
            <UserAdd users={this.state.users} onUserAdd={this.userAddHandler}></UserAdd>
            <ul>{users}</ul>
            </div>
        );
    }
}

export default UserList;