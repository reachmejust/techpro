import React from 'react';

function UserInfo(props) {
    return (
        <div>
        <span>{props.name}</span> | <span>{props.age}</span> | <span><a onClick={props.onRemoveUser}>X</a></span>
        </div>
    );
}

export default UserInfo;