import React from 'react';
import './App.css';
import UserList from './users/UserList';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <UserList></UserList>
      </div>
    );
  }
}

export default App;
