const mongoose = require('mongoose');

const URI = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/techpro-db';

mongoose.connect(URI, {
    useNewUrlParser: true,
    useCreateIndex: true
});
