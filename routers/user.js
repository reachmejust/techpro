const express = require('express');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const auth = require('../middleware/auth');

const router = new express.Router();

router.get('/users/me', auth, async (req, res)=>{
    res.send(req.user);
});
  
router.delete('/users/:userId', async (req, res) => {
    const userId = req.params.userId;
    try {
        const user = await User.findByIdAndRemove(userId);    
        res.send(user);
    } catch (error) {
        res.status(500).send(error);        
    }
});  
  
router.post('/users', async (req, res) => {
    const user = new User(req.body);
    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({user, token});
    } catch (error) {
        res.status(400).send(error.message); 
    }
});
  
router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({user, token});
    } catch (error) {
        res.status(400).send(error);
    }
});  
  
router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token)=>{
            return token.token !== req.token;
        });
        await req.user.save();
        res.send();
    } catch (error) {
        res.status(500).send();
    }
});

router.post('/users/logoutall', auth, async (req, res) => {
    try {
        req.user.tokens =[];
        await req.user.save();
        res.send();
    } catch (error) {
        res.status(500).send();
    }
});

module.exports = router;