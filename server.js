require('./db/mongoose');

const path = require('path');
const express = require('express');

const userRouter = require('./routers/user');

const PORT = process.env.PORT || 4001;

const app = express();

app.use(express.json());
app.use(express.static(path.join(__dirname, 'client', 'build')));

app.use(userRouter);


app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

app.listen(PORT, () => {
  console.log(`Server listening at port ${PORT}.`);
});